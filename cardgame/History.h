//
//  History.h
//  cardgame
//
//  Created by Jonathan Zallas on 1/26/15.
//  Copyright (c) 2015 Zallas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface History : NSObject

- (instancetype)initWithCards:(NSArray*)cards
                   withPoints:(NSInteger)points
                    withMatch:(BOOL)match;

@property(strong, nonatomic) NSArray * cards;

@property(nonatomic) NSInteger points;

@property(nonatomic, getter=isMatch) BOOL match;

@end