//
//  Deck.h
//  cardgame
//
//  Created by Zallas on 12/28/14.
//  Copyright (c) 2014 Zallas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Card.h"

@interface Deck : NSObject

-(void) addCard: (Card *) card atTop:(BOOL) atTop;
-(void) addCard:(Card *) card;
-(Card *) drawRandomCard;

@end