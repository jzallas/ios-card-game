//
//  Card.h
//  cardgame
//
//  Created by Zallas on 12/28/14.
//  Copyright (c) 2014 Zallas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Card: NSObject

@property(strong, nonatomic) NSString * contents;

@property(nonatomic, getter = isChosen) BOOL chosen;

@property(nonatomic, getter = isMatched) BOOL matched;

- (int) match : (NSArray *) otherCards;

@end
