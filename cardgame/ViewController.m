//
//  ViewController.m
//  cardgame
//
//  Created by Zallas on 12/28/14.
//  Copyright (c) 2014 Zallas. All rights reserved.
//

#import "ViewController.h"
#import "PlayingCardDeck.h"
#import "CardMatchingGame.h"

@interface ViewController ()

@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *cardButtons;
@property (weak, nonatomic) IBOutlet UISwitch *modeSwitch;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@property (nonatomic) NSUInteger numberOfCardsToMatch;
@property (weak, nonatomic) IBOutlet UITextView *historyTextView;
@property (strong, nonatomic) CardMatchingGame * game;

@end

static NSString * const MATCHED_MESSAGE = @"Matched %@for %ld points";
static NSString * const MISMATCHED_MESSAGE = @"%@don't match! %ld point penalty!";

@implementation ViewController

-(CardMatchingGame*) game{
    if(!_game){
        _game = [[CardMatchingGame alloc]initWithCardCount:[self.cardButtons count]
                                                 usingDeck:[self createDeck]];
        _game.numberOfCardsToMatch = self.numberOfCardsToMatch;
    }
    return _game;
}
- (IBAction)resetButtonPressed:(UIButton *)sender {
    self.modeSwitch.enabled = true;
    
    self.game = nil;
    [self updateUI];
}

- (IBAction)modeSwitchPressed:(UISwitch *)sender {
    
    if (sender.isOn)
        self.numberOfCardsToMatch = 3;
    else
        self.numberOfCardsToMatch = 2;
    
    self.game.numberOfCardsToMatch = 3;
}

-(PlayingCardDeck *)createDeck{
    return [[PlayingCardDeck alloc]init];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

-(void) updateUI{
    for (int i = 0; i < [self.cardButtons count]; i++){
        
        Card *card = [self.game cardAtIndex:i];
        UIButton *button = self.cardButtons[i];
        
        if (card.isMatched){
            button.alpha = 0.5f;
            [button setTitle:card.contents forState:UIControlStateNormal];
            [button setBackgroundImage:[UIImage imageNamed:@"card_front"] forState:UIControlStateNormal];
            button.enabled = NO;
        } else if(card.isChosen){
            button.alpha = 1.0f;
            [button setTitle:card.contents forState:UIControlStateNormal];
            [button setBackgroundImage:[UIImage imageNamed:@"card_front"] forState:UIControlStateNormal];
            
        } else {
            button.enabled = YES;
            button.alpha = 1.0f;
            [button setTitle:@"" forState:UIControlStateNormal];
            [button setBackgroundImage:[UIImage imageNamed:@"card_back"] forState:UIControlStateNormal];
        }
    }
    
    [self.scoreLabel setText:[NSString stringWithFormat:@"Score : %ld",(long)self.game.score]];
    
    [self displayHistory];
    
}

- (void) displayHistory{
    History * latestHistory = self.game.latestHistory;

    NSString * historyText;
    
    if (latestHistory){

        NSString * cardString = @"";
        
        // create a string from the card contents
        for (Card * card in latestHistory.cards){
            cardString = [cardString stringByAppendingString:card.contents];
        }
        
        // remove the last space
        cardString = [cardString substringToIndex:cardString.length-1];
        

        
        if (latestHistory.isMatch){
            historyText = [NSString stringWithFormat:MATCHED_MESSAGE, cardString, (long)latestHistory.points];
        } else {
            historyText = [NSString stringWithFormat:MISMATCHED_MESSAGE, cardString, (long)latestHistory.points];
        }
        
    }
    
    self.historyTextView.text = historyText;
    self.historyTextView.textAlignment = NSTextAlignmentCenter;

}

- (IBAction)buttonPressed:(UIButton *)sender {
    
    self.modeSwitch.enabled = false;
    
    // get the index of the pressed button
    NSUInteger index = [self.cardButtons indexOfObject:sender];
    
    // notify the game that the card was selected and perform game logic
    [self.game chooseCardAtIndex:index];
    
    // redraw the UI after the game logic
    [self updateUI];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end