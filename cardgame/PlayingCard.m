//
//  PlayingCard.m
//  cardgame
//
//  Created by Zallas on 12/28/14.
//  Copyright (c) 2014 Zallas. All rights reserved.
//
#import "PlayingCard.h"

@implementation PlayingCard

+(NSArray *) validSuits{
    return @[@"♦️",@"♣️",@"♥️",@"♠️"];
}

+(NSUInteger) maxRank{
    return [[PlayingCard rankStrings] count]-1;
}

+(NSArray *) rankStrings{
    return @[@"?",@"A",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"J",@"Q",@"K"];
}

@synthesize suit = _suit;

-(void)setSuit:(NSString *)suit{
    if ([[PlayingCard validSuits] containsObject:suit]){
        _suit = suit;
    }
}

static const int RANK_MATCH_BONUS = 4;

static const int SUIT_MATCH_BONUS = 1;

- (int) match:(NSArray *)otherCards{
    int score = 0;
    for (int i = 0; i < [otherCards count]; i++){
        PlayingCard * card = otherCards[i];
        if (card.rank == self.rank){
            score += 1 * RANK_MATCH_BONUS;
        }
        if ([card.suit isEqualToString:self.suit]){
            score += 1 * SUIT_MATCH_BONUS;
        }
    }
    
    return score;
}

-(NSString *)suit{
    return _suit? _suit : @"?";
}

-(void) setRank:(NSUInteger)rank{
    if (rank <= [PlayingCard maxRank]){
        _rank = rank;
    }
}

-(NSString *) contents{
    return [[PlayingCard rankStrings][self.rank] stringByAppendingString:self.suit];
}

@end