//
//  PlayingCard.h
//  cardgame
//
//  Created by Zallas on 12/28/14.
//  Copyright (c) 2014 Zallas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Card.h"

@interface PlayingCard : Card

@property (strong, nonatomic) NSString * suit;
@property (nonatomic) NSUInteger rank;

+(NSArray *) validSuits;
+(NSUInteger) maxRank;

@end