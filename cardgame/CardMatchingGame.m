//
//  CardMatchingGame.m
//  cardgame
//
//  Created by Zallas on 12/30/14.
//  Copyright (c) 2014 Zallas. All rights reserved.
//

#import "CardMatchingGame.h"

static const int MATCH_BONUS = 4;

static const int MISMATCH_PENALTY = -2;

static const int COST_TO_CHOOSE = -1;

@interface CardMatchingGame()
@property (nonatomic,readwrite) NSInteger score;
@property (nonatomic, strong) NSMutableArray * cards;
@end

@implementation CardMatchingGame

- (NSMutableArray*) cards{
    if (!_cards){
        _cards = [[NSMutableArray alloc]init];
    }
    return _cards;
}


// default is two unless otherwise set to another amount
- (NSUInteger)numberOfCardsToMatch{
    if (_numberOfCardsToMatch < 2)
        _numberOfCardsToMatch = 2;
    return _numberOfCardsToMatch;
}

-(instancetype) initWithCardCount:(NSUInteger)count usingDeck:(Deck *)deck{
    self = [super init];
    
    if (self){
        for(int i = 0; i < count; i++){
            Card * card = [deck drawRandomCard];
            if(card)
                [self.cards addObject:card];
            else{
                self = nil;
                break;
            }
        }
    }
    
    return self;
}

-(instancetype) init{
    return nil;
}

-(Card *) cardAtIndex:(NSUInteger)index{
    if (index < [self.cards count])
        return self.cards[index];
    return nil;
}

-(void) chooseCardAtIndex:(NSUInteger)index{
    // get the card at the index
    Card *card = [self cardAtIndex:index];
    
    // if the index is valid, a card should not be null
    if (card){
        // if the card has not been matched yet
        if(!card.isMatched){
            // if the card was already chosen, un-choose it
            if (card.isChosen)
                card.chosen = NO;
            
            // if this card wasn't chosen, choose it now
            else{
                card.chosen = YES;
                self.score += COST_TO_CHOOSE;
                
                NSArray* chosenCards = [self getChosenCards];
                
                // if enough cards have been chosen, check for points
                if (chosenCards.count == self.numberOfCardsToMatch){
                    NSInteger points = [self calculatePointsFromChosenCards:chosenCards];
                    
                    if (points){
                        // all selected cards will be set to matched
                        for (Card *chosenCard in chosenCards)
                            chosenCard.matched = YES;
                        
                    } else {
                        
                        // there was no match so prepare a mismatch penalty
                        points = MISMATCH_PENALTY * (chosenCards.count-1);
                        
                        // unchoose all cards
                        for (Card *chosenCard in chosenCards){
                            chosenCard.matched = NO;
                            chosenCard.chosen = NO;
                        }
                        
                        // the last card that was chosen should remain chosen
                        card.chosen = YES;
                    }
                    
                    self.score +=points;
                    
                    BOOL wasMatch = (points > 0);
                    
                    self.latestHistory = nil;
                    self.latestHistory = [[History alloc] initWithCards:chosenCards withPoints:points withMatch:wasMatch];
                }
            }
        }
    }
}

-(int) calculatePointsFromChosenCards:(NSArray*)chosenCards{

    int totalCalculatedPoints = 0;
    
    for (int i = 0; i < chosenCards.count - 1; i++){
        
        Card * currentCard = chosenCards[i];
        
        // calculate points for each card versus the remaining potential pairs
        // for example: if we had cards 0, 1, 2, 3 then the points will be...
        // 0 matched with 1, 2, 3
        // plus
        // 1 matched with 2, 3
        // plus
        // 2 matched with 3
        
        NSUInteger start = i + 1;
        NSUInteger length = chosenCards.count - i - 1;
        NSRange subRange = NSMakeRange(start, length);

        NSArray* cardSubArray = [chosenCards subarrayWithRange:subRange];
        
        totalCalculatedPoints += [currentCard match:cardSubArray];
    }
    
    return totalCalculatedPoints * MATCH_BONUS;
    
}

-(NSArray*) getChosenCards{
    NSMutableArray * chosenCards = [[NSMutableArray alloc]init];
    
    for(Card* card in self.cards){
        if (card.isChosen && !card.isMatched)
            [chosenCards addObject:card];
    }
    
    return chosenCards;
}

@end

