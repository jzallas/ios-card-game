//
//  History.m
//  cardgame
//
//  Created by Jonathan Zallas on 1/26/15.
//  Copyright (c) 2015 Zallas. All rights reserved.
//

#import "History.h"

@implementation History

- (instancetype)initWithCards:(NSArray*)cards
                   withPoints:(NSInteger)points
                    withMatch:(BOOL)match{
    self = [super init];
    if (self) {
        self.cards = cards;
        self.points = points;
        self.match = match;
    }
    return self;
}

@end
