//
//  CardMatchingGame.h
//  cardgame
//
//  Created by Zallas on 12/30/14.
//  Copyright (c) 2014 Zallas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Deck.h"
#import "History.h"

@interface CardMatchingGame : NSObject

- (instancetype) initWithCardCount:(NSUInteger)count usingDeck:(Deck *) deck;

-(void) chooseCardAtIndex: (NSUInteger) index;

-(Card *) cardAtIndex:(NSUInteger) index;

@property(nonatomic,readonly) NSInteger score;

@property(nonatomic) NSUInteger numberOfCardsToMatch;

@property(nonatomic, strong) History * latestHistory;

-(void) resetGameUsingDeck:(Deck*) deck;

@end